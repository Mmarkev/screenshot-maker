//Configuring node.js app on local server
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')
const port = 3000

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
app.use(cors())

app.get('/', (req, res) => {
    res.send("Server is working")
})

app.listen(port, () => {
    console.log(`Server is runing on port ${port}`)
})


//Requires
const screenshotmachine = require('screenshotmachine');
const { google, jobs_v3 } = require('googleapis');
const path = require('path');
const fs = require('fs');
//Data about webpages from JSON
const webpages = require('./webpages.json');

//API info
var customerKey = '12b664';
secretPhrase = '';

const CLIENT_ID = '585421618367-bd6ots1b4gfo2o2t2cc4qipuetfa2a8e.apps.googleusercontent.com';
const CLIENT_SECRET = 'fSPOoZUNuLkdHSICFBYyXkXA';
const REDIRECT_URI = 'https://developers.google.com/oauthplayground';
const REFRESH_TOKEN = '1//04OcY2uUPYwm6CgYIARAAGAQSNwF-L9IrD1YdFyHYRQuNlqOkO8vrSEG320mOFuG6WnHvcS9tfv2d1JXoLA-WubskoxjawBOEhTE';

const oauth2Client = new google.auth.OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URI);
oauth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });
const drive = google.drive({ version: 'v3', auth: oauth2Client });

//Saving each page screenshot to dir
function createFiles() {
    for (let webpage of webpages) {
        var apiUrl = screenshotmachine.generateScreenshotApiUrl(customerKey, secretPhrase, webpage.options);
        var fileName = webpage.id + '_' + webpage.name + '.jpg';
        screenshotmachine.readScreenshot(apiUrl).pipe(fs.createWriteStream(fileName).on('close', function () {
            console.log('Screenshot saved as ' + fileName);
        }));
    }
}

//Uploading screenshots to Google Drive folder
async function uploadFiles() {
    try {
        var folderId = '1XB7vWEmfcgYVv1VTv-wlZ7OUcgDSLoF2';
        for (let webpage of webpages) {
            var fileName = webpage.id + '_' + webpage.name + '.jpg';
            var filePath = path.join(__dirname, fileName);

            var fileMetadata = {
                'name': fileName,
                parents: [folderId]
            };
            var media = {
                mimeType: 'image/jpg',
                body: fs.createReadStream(filePath)
            };

            const response = await drive.files.create({
                resource: fileMetadata,
                media: media,
                uploadType: 'media',
                fields: 'id'
            });
            console.log(response.data);
        }
    } catch (error) {
        console.log(error.message);
    }
}

createFiles();
setTimeout(uploadFiles, 3000);